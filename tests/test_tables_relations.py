import os
from os.path import join as pjoin

import pandas as pd

from src.byproducts.dico_snds import table_schema_to_snds_graph, DICO_EDGES_CSV, DICO_NODES_CSV
from src.constants import DICO_SNDS_DIR, ROOT_DIR


def test_no_isolated_table():
    os.makedirs(DICO_SNDS_DIR, exist_ok=True)
    table_schema_to_snds_graph(ROOT_DIR, products=None)

    df_edges = pd.read_csv(pjoin(DICO_SNDS_DIR, DICO_EDGES_CSV))
    df_nodes = pd.read_csv(pjoin(DICO_SNDS_DIR, DICO_NODES_CSV))
    

    connected_tables = set(df_edges["source"]).union(set(df_edges["target"]))
    
    # Exclude DA_PRA_R table which is known to be isolated
    df_nodes_filtered = df_nodes[df_nodes["name"] != "DA_PRA_R"]

    print(
        f"""Tables non connectées : \n{df_nodes_filtered.loc[~df_nodes_filtered["index"].isin(connected_tables), ['name', 'description']]}"""
    )

    assert len(connected_tables) == len(df_nodes_filtered)
