# Schéma du SNDS

Le SNDS est une collection de base de données, difficile à documenter et à manipuler.

Ce schéma du SNDS **centralise** une description du SNDS sous un format **standard**.
 
 
## Utilité

Ce schéma permet de générer des documentations dans plusieurs formats, actuellement :
- un [dictionnaire interactif](http://dico-snds.health-data-hub.fr/) des variables et des liens entre tables,
- une [documentation textuelle](https://documentation-snds.health-data-hub.fr/tables/).

Il permet également de générer une version [synthétiques du SNDS](https://documentation-snds.health-data-hub.fr/ressources/donnees_synthetiques.html).


## Modifier le schéma

Le schéma est formalisé en suivant la spécification Table Schema, présentée dans ce [document](documentation/Table-Schema.md).

Vos propositions de corrections et d'améliorations sont bienvenues.

Pour cela vous pouvez :
- éditer directement les fichiers json dans le dossier [schemas](schemas) et ouvrir une `merge-request`,
- ouvrir une `issue`,
- écrire à l'équipe open source du Health Data Hub qui développe et maintient ce projet <opensource@health-data-hub.fr>

## Installation du projet pour générer les produits dérivés

**Dépendances** : python3, virtualenv, Docker, Docker-Compose, git, npm

Activation d'un environnement virtuel Python avec les dépendances installées  

    ./create_vitualenv.sh
    source venv/bin/activate

Installation de la librairie javascript table-schema-to-markdown

    npm install -g @opendataschema/table-schema-to-markdown 

Utilisation 

    ./main.py -h


Pour des explications détaillées, se référer au fichier [INSTALL.md](INSTALL.md)

## Comprendre les tests

  > **Test Byproducts**
    Cette partie permet de tester que les produits dérivés sont générés correctement, à la fois pour un sous-ensemble de schémas de test (test_generate_byproducts_tests) mais aussi sur l'ensemble des schémas (test_generate_byproducts_root).
    
    - test_generated_byproducts_equal_files : compare les produits dérivés générés pour les schémas de test aux produits dérivés attendus pour ces mêmes schémas. Ainsi, si le comportement de génération des produits dérivés évolue, il faut égalemnt faire évoluer les produits dérivés attendus.
    - test_update_all_byproducts_locally : ce test permet de s'assurer que la mise à jour des autres repositories (documentation-snds, dico-snds) pourront bien être mis à jour

  > **Test Nomenclatures**
    Cette partie permet de tester la cohérence des nomenclatures et des schémas JSON associés à celles-ci.
    
    - test_all_nomenclatures_have_schema : vérifie que chaque nomenclature a bien un schéma JSON associé.
    - test_validate_nomenclature_schema : teste que le schéma des nomenclatures est valide selon le standard Table Schema.
    - test_nomenclature_schema_name_equal_file_name : teste que le nom indiqué dans le champ "name" du schéma correspond bien au nom du fichier de nomenclature.
    - test_nomenclature_primary_keys_is_unique : vérifie que la ou les colonnes identifiée(s) comme clé primaire pour la nomenclature ont bien des valeurs uniques.
    - test_nomenclature_have_title : s'assure que chaque nomenclature a bien une description dans le champ title de son schéma JSON.
    - test_nomenclatures_have_one_field_with_role_label : pour chaque nomenclature, une des colonnes doit avoir la mention "role : label" dans le schéma pour indiquer qu'il s'agit du libellé de la nomenclature.
    - test_nomenclatures_have_one_field__with_role_code : pour chaque nomenclature, une des colonnes doit avoir la mention "role : code" dans le schéma pour indiquer qu'il s'agit du code de la nomenclature.
  
  > **Test Tables Relations**
    Cette partie teste les informations de jointures entre les tables contenues dans les shémas.

    - test_no_isolated_table : vérifie qu'il n'y a pas de table isolée du reste des tables du SNDS en suivant les clés de jointure (foreign keys) contenues dans les schémas. Ce test ne concerne pas certaines tables isolées par définition comme les tables "OPENDATA" et "EGB".

  > **Test TableSchema Validity**
    Cette partie permet de vérifier que les schémas sont valides par rapport au standard Table Schéma ainsi que d'autres règles prédéfinies.

    - test_tableschema_is_valid : vérifie que le schéma de chaque table est valide selon le standard Table Schema.
    - test_tableschema_name_and_product : teste que le nom indiqué dans la variable "name" correspond bien au nom de la table (nom du fichier du schéma) et que le nom indiqué dans la variable "product" correspond bien au produit SNDS de la table (nom du répertoire dans lequel se trouve le fichier du schéma).
    - test_get_all_schema_path_return_all_schemas : s'assure que la fonction "get_all_schema_path" fonctionne correctement et renvoit bien la liste entière des schémas JSON.
    - test_tableschema_contains_all_fields : vérifie que chaque schéma des tables contient tous les champs obligatoires pour la description de la table et des variables.

  > **Test Values Validity**
    Cette partie vérifie que les valeurs indiqués dans les schémas JSON pour certains cas particuliers sont correctes.

    - test_length_values_are_valid : s'assure que la valeur prise par la variable "length" a une valeur décimale quand le type du champ associé est "NUMBER" ou un nombre entier quand le type du champ associé est "INTEGER".
    - test_date_type_values_are_valid : vérifie que les champs correspondant à des dates ont bien le type "date" ou "yearmonth" associé.

## Historique 

La première version du schéma provient d'un travail 
[archivé ici](https://gitlab.com/healthdatahub/dico-snds-creation-archive), 
basé sur des documents de la [Caisse Nationale d'Assurance Maladie](https://assurance-maladie.ameli.fr/qui-sommes-nous). 


## Mentions

Ce projet a été initié dans le cadre du programme 
[« Entrepreneurs d'intérêt général »](https://entrepreneur-interet-general.etalab.gouv.fr/), 
pour le projet [Open Chronic](https://entrepreneur-interet-general.etalab.gouv.fr/defis/2019/openchronic.html) et développé par Pierre-Alain Jachiet (actuellement à l'HAS). 

Ce projet est maintenu par le [Health Data Hub](https://www.health-data-hub.fr/).
