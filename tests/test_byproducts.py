import os
from os.path import join as pjoin

import pytest

from src.byproducts.update_byproducts_repositories import update_all_byproducts
from src.constants import TESTS_DIR, ROOT_DIR, BYPRODUCTS_DIR

EXPECTED_BYPRODUCTS_DIR = 'expected_byproducts'


def test_generate_byproducts_root(generate_byproducts_root):
    pass


def test_generate_byproducts_tests(generate_byproducts_tests):
    pass


def get_files_in_directory(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            yield root, file


@pytest.mark.parametrize('root,file', get_files_in_directory(pjoin(TESTS_DIR, EXPECTED_BYPRODUCTS_DIR)))
def test_generated_byproducts_equal_files(root, file):
    expected_file_path = os.path.join(root, file)
    actual_file_path = expected_file_path.replace(EXPECTED_BYPRODUCTS_DIR, BYPRODUCTS_DIR)
    with open(actual_file_path, encoding="utf-8") as f:
        actual = f.read().split("\n")
    with open(expected_file_path, encoding="utf-8") as f:
        expected = f.read().split("\n")
    for expected_line, actual_line in zip(expected, actual):
        assert expected_line == actual_line


import logging

def update_all_byproducts(local, work_dir) -> None:
    if not local and GITLAB_TOKEN is None:
        raise Exception("GITLAB TOKEN is not set. This is expected, if we are not executing on GitLab-CI runners, "
                        "for protected branches. Exiting, as we cannot automatically update byproducts.")

    last_commit_id_str = get_last_commit_id_str()
    logging.debug(f"Last commit ID: {last_commit_id_str}")

    update_errors = 0
    update_errors += update_byproduct_repository(
        byproduct_repository='documentation-snds',
        local_to_byproduct_directories=[
            ('schemas-md', pjoin('snds', 'tables', '.schemas'))],
        local_to_byproduct_files=[],
        last_commit_id_str=last_commit_id_str,
        byproduct_project_id=11935953,
        work_dir=work_dir,
        automatic_merge=True,
        local=local
    )

    logging.debug(f"Errors after updating documentation-snds: {update_errors}")

    update_errors += update_byproduct_repository(
        byproduct_repository='dico-snds',
        local_to_byproduct_directories=[
            ('nomenclatures', pjoin('app', 'app_data', 'nomenclatures'))
        ],
        local_to_byproduct_files=[(csv_file, pjoin('app', 'app_data', csv_file)) for csv_file in DICO_CSV_FILES],
        last_commit_id_str=last_commit_id_str,
        byproduct_project_id=11925754,
        work_dir=work_dir,
        automatic_merge=True,
        local=local
    )

    logging.debug(f"Errors after updating dico-snds: {update_errors}")

    if update_errors:
        raise Exception("Their were {} errors in byproduct update. Look for ERROR logs.".format(update_errors))
